Param(  
    [string]$nerdFontURL,
    [string]$nerdFontPath
)

if ([string]::IsNullOrWhiteSpace($nerdFontURL) -eq $false){   
    Write-Host "Nerd Font Specified"

    Write-Host "Installing Nerd Font"# Font directory
    
    #Check if font directory exists
    $fontPathCheck = Test-Path -Path $nerdFontPath
    
    #Create logging directory if it doesn't exist
    If (!($fontPathCheck)) {
        New-Item -ItemType Directory $nerdFontPath -Force
    }
    
    Write-Host "Unzipping Nerd Font..."
    Invoke-WebRequest -Uri ""$nerdFontURL"" -OutFile ""$($nerdFontPath)\font.zip""

    Expand-Archive -Path "$($nerdFontPath)\font.zip" -Destination $($nerdFontPath) -Force
    $systemFontsPath = "C:\Windows\Fonts"
    
    foreach($fontFile in Get-ChildItem $nerdFontPath -Include '*.ttf','*.ttc','*.otf' -recurse ) {
        $targetPath = Join-Path $systemFontsPath $fontFile.Name
        if(Test-Path -Path $targetPath){
            $fontFile.Name + " is already installed!"
        }
        else {
            "Installing font " + $fontFile.Name
            
            #Extract Font information for Reqistry 
            $ShellFolder = (New-Object -COMObject Shell.Application).Namespace($nerdFontPath)
            $ShellFile = $ShellFolder.ParseName($fontFile.name)
            $ShellFileType = $ShellFolder.GetDetailsOf($ShellFile, 2)
    
            #Set the $FontType Variable
            If ($ShellFileType -Like '*TrueType font file*') {$FontType = '(TrueType)'}
                
            #Update Registry and copy font to font directory
            $RegName = $ShellFolder.GetDetailsOf($ShellFile, 21) + ' ' + $FontType
            New-ItemProperty -Name $RegName -Path "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\Fonts" -PropertyType string -Value $fontFile.name -Force | out-null
            Copy-item $fontFile.FullName -Destination $systemFontsPath
            "Done"
        }
    }
}

