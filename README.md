# About

This repo contains some scripts to automate:

- Installation of apps
- Installation of Windows Features
- Installing a Nerd Font

:warning: To be used **after** a new Windows install

If you often install Windows, you may find this repo useful.

## Usage

There are currently 3 main scripts: `app-installer.ps1`, `font-installer.ps1` and `run-me.ps1`.

**All variables are declared in `run-me.ps1`**

1. Download the zip file ([link][zip-link]) and extract it into a folder
2. Close all applications
3. Open PowerShell with "Run as Administrator"
4. Assuming you download and extract the files in your `Downloads` folder, do:

  `"cd %USERPROFILE%\Downloads\windows-dev-setup-installer\"`
  `.\run-me.ps1`

## Applications To Be Installed

| App                    |
|------------------------|
| 7-Zip                  |
| Clink                  |
| Discord                |
| Docker Desktop         |
| Git Bash               |
| Google Chrome          |
| Meslo Nerd Font        |
| Notepad++              |
| Oh-My-Posh             |
| OpenJDK                |
| Python3                |
| Virtualbox             |
| Virtualbox Additions   |
| Visual Studio Code     |
| VLC Media Player       |
| Windows Terminal       |

## Windows Features To Be Installed

| Feature                        |
|--------------------------------|
| Simple TCP Services (Echo etc) |
| Telnet                         |
| TFTP                           |
| NFS Client                     |
| Virtual Machine Platform       |
| WSL2                           |

## Important Notice

:warning: **Only tested on Windows 11 64-bit!**.

This repo has the following requirements:

- You have a stable internet connection

## To-Do

- [ ] Switch to check architecture and memory available
- [ ] Switches to check if apps are already installed
- [ ] Cleanup scripts
- [ ] Prettify logging outputs

## Copyrights and Licenses

This project uses some third-party codes and binaries, which have their own licenses.

I do not own any trademark or copyright of the application software, for example Adobe, Google, JRE, etc. Please carefully read their user and distribution terms and agreements.

[zip-link]: https://gitlab.com/N3UR0515/windows-dev-setup-installer/-/releases/latest
