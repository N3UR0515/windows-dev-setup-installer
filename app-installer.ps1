Param(  
    [string]$chocolateyAppList,
    [string]$dismFeatureList
)

function check_if_installed($app) {
    $software = $app;
    $installed = ((Get-ItemProperty HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\*).DisplayName -Match $software).Length -gt 0
    If (-Not $installed) {
        Write-Host "'$software' is not installed.";
    }
    else {
        Write-Host "'$software' is installed."
    }
    return $installed
}

try{
    choco config get cacheLocation
}catch{
    Write-Output "Chocolatey not detected, trying to install now"
    Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

if ([string]::IsNullOrWhiteSpace($dismFeatureList) -eq $false){
    Write-Host "DISM Features Specified"    

    $featuresToInstall = $dismFeatureList -split "," | ForEach-Object { "$($_.Trim())" }

    foreach ($feature in $featuresToInstall)
    {
        Write-Host "Installing $feature"
        & choco install $feature /source windowsfeatures /y | Write-Output
    }
}

if ([string]::IsNullOrWhiteSpace($chocolateyAppList) -eq $false){   
    Write-Host "Chocolatey Apps Specified"  
    
    $appsToInstall = $chocolateyAppList -split "," | ForEach-Object { "$($_.Trim())" }

    foreach ($app in $appsToInstall)
    {
        Write-Host "Checking if $app is installed..."
        $app_installed = check_if_installed($app)
        if ($app_installed -eq $false){
            Write-Host "Installing $app..."
            & choco install $app /y | Write-Output
        }
        else {
            Write-Host "$app already installed, skipping."
        }
        
    }
}
