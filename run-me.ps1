#Requires -RunAsAdministrator

$chocolateyAppList = "7zip,adobereader,clink,discord,docker-desktop,git,googlechrome,notepadplusplus,openjdk,oh-my-posh,python3,virtualbox,virtualbox-guest-additions-guest.install,vlc,vscode,microsoft-windows-terminal,wsl2"
$dismFeatureList = "ClientForNFS-Infrastructure,Microsoft-Windows-Subsystem-Linux,ServicesForNFS-ClientOnly,SimpleTCP,TelnetClient,TFTP,VirtualMachinePlatform"
$nerdFontURL = "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Meslo.zip"
$nerdFontPath = "./nerd-font"

Write-Output "Starting installation script. Make sure you know what is going to happen!"

# Show Warning
Write-Output "Before you install any package, please read their license agreements!"
Write-Output "When you download and install a package, you already agree to their terms."
Write-Output "Please read them carefully!"

$agree = Read-Host -Prompt "Do you want to continue? (y/n)"

if ($agree -eq "y") {
    Write-Output "Continuing..."
} else {
    Write-Output "Exiting..."
    exit
}

Write-Output "Installing Windows Features..."
Invoke-Expression ".\app-installer.ps1 ""$dismFeatureList"" ""$chocolateyAppList"""

Write-Output "Installing Font..."
Invoke-Expression ".\font-installer.ps1 ""$nerdFontURL"" ""$nerdFontPath"""

Write-Output "Installation complete!"